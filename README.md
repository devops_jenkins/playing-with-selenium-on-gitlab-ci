# Playing with Selenium on GitLab CI

A sandbox project built while working out the kinks in https://gitlab.com/gitlab-org/gitlab-ce/issues/6065 and figuring out how to automatically take screenshots after each step/command/action in a test.

# Running things

You can just pop this project onto GitLab CI to see it in action.

### Running locally/manually

Start up a local Selenium server, see the "Start your own Selenium sever" section below

Then start the GitLab Selenium proxy server, see https://gitlab.com/gitlab-org/gitlab-selenium-server#general-usage

Then start some Selenium tests pointing at the GitLab Selenium proxy server

```
SELENIUM_REMOTE_URL=http://localhost:4545/wd/hub npm test
```

# Setup a GitLab CI Runner (with GDK)

 1. Install [Docker for Mac](https://www.docker.com/docker-mac), [Docker for Windows](https://www.docker.com/docker-windows), or [Docker Toobox](https://www.docker.com/products/docker-toolbox)
 1. Install GitLab CI Multi Runner, https://docs.gitlab.com/runner/install/
 1. **If using Docker Toolbox**, Create a Docker machine, `docker-machine create --driver virtualbox gl-ci-runner1` or if you already have one `docker-machine start gl-ci-runner1`
 1. **If using Docker Toolbox**, Setup the environment context, `docker-machine env gl-ci-runner1` (make sure to run the command at the bottom of the output of that command)
 1. Run `docker info` to see if the daemon is running now
 1. Register a runner with the `docker` executor in the same terminal context,
    - http://docs.gitlab.com/runner/register/
    - https://docs.gitlab.com/runner/executors/docker.html

    ```
    $ gitlab-ci-multi-runner register

    Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
    http://localhost:3000/
    Please enter the gitlab-ci token for this runner:
    xxx
    Please enter the gitlab-ci description for this runner:
    [eric-macbook]: some-docker-runner3
    Please enter the gitlab-ci tags for this runner (comma separated):
    docker,shared
    Whether to run untagged builds [true/false]:
    [false]:
    Registering runner... succeeded                     runner=8Aqg65bP
    Please enter the executor: virtualbox, docker+machine, docker-ssh+machine, kubernetes, docker-ssh, shell, ssh, docker, parallels:
    docker
    Please enter the default Docker image (e.g. ruby:2.1):
    node:latest
    Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
    ```


# Random Notes

- https://w3c.github.io/webdriver/webdriver-spec.html
   - Screen capture, https://w3c.github.io/webdriver/webdriver-spec.html#screen-capture
 - https://seleniumhq.github.io/docs/

##### Logging

You can fetch logs by...

Use the [`selenium-webdriver`](https://www.npmjs.com/package/selenium-webdriver) Node.js package API:

```
const webdriver = require('selenium-webdriver');

const prefs = new webdriver.logging.Preferences();
prefs.setLevel(webdriver.logging.Type.BROWSER, webdriver.logging.Level.ALL);
prefs.setLevel(webdriver.logging.Type.CLIENT, webdriver.logging.Level.ALL);
prefs.setLevel(webdriver.logging.Type.DRIVER, webdriver.logging.Level.ALL);
prefs.setLevel(webdriver.logging.Type.PERFORMANCE, webdriver.logging.Level.ALL);
prefs.setLevel(webdriver.logging.Type.SERVER, webdriver.logging.Level.ALL);
const capabilities = webdriver.Capabilities.chrome();
capabilities.setLoggingPrefs(prefs);

const driver = new webdriver.Builder()
   // ...
   .withCapabilities(capabilities)
   .build();

// `type` can be one of these ['BROWSER', 'CLIENT', 'DRIVER', 'PERFORMANCE', 'SERVER']
driver.manage().logs().get(webdriver.logging.Type[type])
```

Or you can make a request directly to the Selenium server:

```
// Get available log types
GET http://localhost:4444/wd/hub/session/${sessionId}/log/types

// Returns JSON log
POST http://localhost:4444/wd/hub/session/${sessionId}/log with { type: '...' }
```


- https://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/lib/logging.html
- http://elementalselenium.com/tips/54-logging
- https://github.com/SeleniumHQ/selenium/wiki/JsonWireProtocol#command-summary
- https://github.com/SeleniumHQ/selenium/wiki/Logging


##### Screenshots after certain action/commands

Sauce Labs and BrowserStack automagically capture screenshots when you do certain actions/commands like clicking around.

There doesn't seem to be an extension/plugin or hook on the Selenium server to know when a certain action/command was made.

The best way to tackle this seems to be a proxy that intercepts requests, first sends off a request to take a screenshot, and then forwards on the request.

- https://support.saucelabs.com/hc/en-us/articles/225265388-Selenium-Commands-that-Trigger-Screenshots


##### Capture video

We could use `selenium/standalone-chrome-debug`(https://github.com/SeleniumHQ/docker-selenium#docker-images-for-selenium-standalone-server-hub-and-node-configurations-with-chrome-and-firefox) which includes a VNC server.
Then use whatever utility to capture a VNC server.

Or with non-headless browsers, install `ffmpeg` alongside the machine and capture the desktop.

You could use Zalenium (extension for Selenium Grid)
 - https://github.com/zalando/zalenium
 - From https://github.com/elgalu/docker-selenium/issues/77

Related links

 - https://stackoverflow.com/questions/18333829/is-there-any-video-recorder-for-selenium-webdriver
    - http://www.seleniummonster.com/boost-up-your-selenium-tests-with-video-recording-capability/
 - https://medium.com/@liviu.lupei/video-recording-of-selenium-webdriver-97638cf6862e
 - https://www.joecolantonio.com/2015/09/28/how-to-record-video-for-your-selenium-tests-using-ffmpeg/




# FAQ

### Use a different Selenium server/hub

Define the `SELENIUM_REMOTE_URL` environment variable. For GitLab CI, we define it in `.gitlab-ci.yml`

ex.
```
SELENIUM_REMOTE_URL=http://username:access_token@ondemand.saucelabs.com:80/wd/hub npm test
```

#### Start your own Selenium sever

 1. Download your driver of choice
    - For example Chrome, https://sites.google.com/a/chromium.org/chromedriver/downloads and put it in `/usr/local/bin` to get it on your `PATH`
 1. Download Selenium server, http://docs.seleniumhq.org/download/
 1. Start the server
    ```
    java -Dwebdriver.chrome.driver=$(which chromedriver) -jar /Users/eric/Downloads/selenium-server-standalone-3.4.0.jar -port 4545
    ```
 1. Access the server with `http://localhost:4545/wd/hub`

### Running into `dial unix /var/run/docker.sock: connect: no such file or directory` when running a job

```
ERROR: Preparation failed: Get http://unix.sock/v1.18/version: dial unix /var/run/docker.sock: connect: no such file or directory
```

Make sure `docker info` works before you register the runner.
Create a machine and run the env command, then register the runner in that same terminal context (see setup above)

Relevant issues, https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/1157, https://github.com/docker/toolbox/issues/296
